yum install -y \
https://repo.ius.io/ius-release-el7.rpm \
https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y https://rpms.remirepo.net/enterprise/remi-release-7.rpm

yum update -y
yum install -y httpd24u httpd24u-mod_ssl


yum -y install yum-utils
yum-config-manager --disable 'remi-php*'
yum-config-manager --enable remi-php82

yum install -y php php-cli php-pdo php-mysqlnd
yum install -y unzip curl wget lsof bind-utils bash-completion mlocate bzip2 tcpdump jq
yum install -y policycoreutils-python tree
yum install -y php-gd php-mbstring php-intl php-xml php-json php-ldap
yum install -y php-pecl-xmlrpc php-imap php-curl php-pecl-zip php-opcache

systemctl enable httpd --now
firewall-cmd --add-service=http --add-service=https --perm
firewall-cmd --reload

yum install -y mariadb104-server


systemctl enable mariadb --now
mariadb -uroot < /vagrant/mariadb_secure_installation.sql
systemctl restart httpd

if [[ ! -f /var/www/html/glpi ]]
then
    tar xzf /vagrant/glpi-10.0.8.tgz --directory=/var/www/html
    chown -R apache: /var/www/html/glpi
    cp /vagrant/glpi.boubou.lan.* /etc/ssl/certs/
    cp /vagrant/*.conf /etc/httpd/conf.d/
    setsebool -P httpd_can_network_connect on
    setsebool -P httpd_can_network_connect_db on
    setsebool -P httpd_can_sendmail on
    semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/glpi/files(/.*)?"
    restorecon -R /var/www/html/glpi/files
    semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/glpi/config(/.*)?"
    semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/glpi/marketplace(/.*)?"
    restorecon -R /var/www/html/glpi/config
    restorecon -R /var/www/html/glpi/marketplace
    systemctl restart httpd

    yum install -y mod_auth_gssapi krb5-workstation.x86_64 gssproxy
    cp /vagrant/glpi_centos_all.keytab /etc/gssproxy
    cat << EOF > /etc/gssproxy/80-httpd.conf
[service/HTTP]
  mechs = krb5
  cred_store = keytab:/etc/gssproxy/glpi_centos_al.keytab
  cred_store = ccache:/var/lib/gssproxy/clients/krb5cc_%U
  euid = apache
EOF
    systemctl restart gssproxy.service
    systemctl enable gssproxy.service
    cat << EOF > /etc/systemd/system/httpd.service
.include /lib/systemd/system/httpd.service
[Service]
Environment=GSS_USE_PROXY=1
EOF
    systemctl daemon-reload
    systemctl restart httpd
    # à adapter
    # nmcli connection modify "System eth1" ipv4.ignore-auto-dns yes ipv4.dns 192.168.56.105
    # nmcli connection modify "System eth1" ipv4.dns-search "lab.lan"
    # systemctl restart NetworkManager
    /usr/bin/cp /vagrant/krb5.conf /etc
    # enregistrer le ticket kerberos, klist pour le visualiser
    env KRB5_TRACE=/dev/stdout kinit -V -k -t /etc/gssproxy/glpi_centos_all.keytab HTTP/glpi@LAB.LAN
fi