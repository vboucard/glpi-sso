# Mettre à jour GLPI
Garder l'ancienne installation :
```
systemctl stop httpd
cd /var/www/html # à adapter selon votre installation
php glpi/bin/console db:check
mv glpi{,.old}
tar xzf /tmp/glpi-xxx.tgz --directory=/var/www/html/
rsync -avz glpi.old/files glpi
rsync -avz glpi.old/config glpi
rsync -avz glpi.old/marketplace glpi
rsync -avz glpi.old/plugins glpi
# peut-être chown -R apache: /var/www/html/glpi
# si selinux : restorecon -R /var/www/html/glpi
systemctl start httpd
```
Penser à réactiver les plugins, voire à les mettre à jour le cas échéant.
# SSO

Projet utilisant vagrant pour installer GLPI sur CentOS 7 et AlmaLinux 8 et pour tester l'intégration SSO, et éventuellement les différences entre les deux systèmes.

## Lectures
https://woshub.com/create-kerberos-keytab-file-spn/

https://www.ibm.com/docs/en/sva/9.0.1?topic=sign-map-kerberos-principal-active-directory-user

https://stackoverflow.com/a/49304368

## Générer le fichier keytab sur l'AD

Créer l'utilisateur, ici glpi_centos, et s'assurer d'avoir un enregsitrement DNS pour le serveur, puis

```
ktpass -princ HTTP/glpi@BOUBOU.LAN -mapuser glpi_centos@BOUBOU.LAN -crypto all -ptype KRB5_NT_PRINCIPAL -out glpi_centos_all.keytab -pass *
```

## Configurer le serveur HTTPD (CentOS 7)
### Préparer l'environnement
Adapter le chemin de cla clé ("keytab:")

```
yum install -y mod_auth_gssapi krb5-workstation gssproxy
cat << EOF > /etc/gssproxy/80-httpd.conf
[service/HTTP]
  mechs = krb5
  cred_store = keytab:/etc/gssproxy/glpi_centos_all.keytab
  cred_store = ccache:/var/lib/gssproxy/clients/krb5cc_%U
  euid = apache
EOF
systemctl restart gssproxy.service
systemctl enable gssproxy.service
cat << EOF > /etc/systemd/system/httpd.service
.include /lib/systemd/system/httpd.service
[Service]
Environment=GSS_USE_PROXY=1
EOF
systemctl daemon-reload
systemctl restart httpd.service
```
### Configurer krb5
Adapter le domaine (realm) et le serveur kerberos
```
cat << EOF > /etc/krb5.conf
# To opt out of the system crypto-policies configuration of krb5, remove the
# symlink at /etc/krb5.conf.d/crypto-policies which will not be recreated.
includedir /etc/krb5.conf.d/

[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

[libdefaults]
    dns_lookup_realm = false
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false
    pkinit_anchors = FILE:/etc/pki/tls/certs/ca-bundle.crt
    spake_preauth_groups = edwards25519
    default_realm = BOUBOU.LAN
    default_ccache_name = KEYRING:persistent:%{uid}

[realms]
BOUBOU.LAN = {
    kdc = kerberos.boubou.lan
    admin_server = kerberos.boubou.lan
}

[domain_realm]
.boubou.lan = BOUBOU.LAN
boubou.lan = BOUBOU.LAN
EOF
```
### Intégrer le fichier de clé
Adapter le chemin et le nom du serveur (doit être le même que celui utilisé lors de la génération de la clé)
```
env KRB5_TRACE=/dev/stdout kinit -V -k -t /etc/gssproxy/glpi_centos_all.keytab HTTP/glpi@BOUBOU.LAN
```

`klist` pour vérifier.

